package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

import model.data_structures.ListaEncadenada.IteratorLista;

public class Stack<T> implements IStack<T> {

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	private ListaEncadenada<T> Stack;


	public Stack()
	{
		Stack = new ListaEncadenada<T> ();
	}

	@Override
	public boolean isEmpty() {

		return Stack.getSize()==0;	}

	@Override
	public int size() {
		return Stack.getSize();
	}

	@Override
	public void push(T t) {
		Stack.add(t);

	}

	@Override
	public T pop() {
		T Eliminado= Stack.darPrimero().darDato();
		Stack.removeFirst();
		return Eliminado;
	}

	@Override
	public IteratorStack<T> iterator() {

		return new IteratorStack<T>(Stack.darPrimero());
	}
	public static class IteratorStack<T> implements Iterator<T>{


		private Nodo<T> proximo;


		public IteratorStack(Nodo<T> primero) {

			proximo = primero;
		}	
		@Override
		public boolean hasNext() {
			return proximo != null;
		}

		@Override
		public T next() {
			if(proximo == null)
				throw new NoSuchElementException("No hay proximo");

			T elemento = proximo.darDato();
			proximo = proximo.darSiguiente();
			return elemento;
		}
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	@Override
	public ListaEncadenada<T> darLista() {
		return Stack;
	}
}




