package model.data_structures;

public  interface IListaEncadenada<T> extends Iterable<T>{

	public Integer getSize();

	public boolean addAtEnd(T dato);

	public boolean add(T dato);

	public void removeFirst();

	public void removeAtEnd();

	public T getElement(int i);

}
