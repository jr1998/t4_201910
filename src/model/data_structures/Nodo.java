package model.data_structures;

public class Nodo<T> {

	private T dato;

	private Nodo<T> siguiente;



	public Nodo(T nDato){
		dato = nDato;
		siguiente = null;


	}

	public Nodo<T> darSiguiente() {
		return  siguiente;
	}

	public void cambiarSiguiente(Nodo<T> dato) {
		siguiente = (Nodo<T>) dato;
	}
	public T darDato() {
		return dato;
	}
}
