package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

import model.data_structures.ListaEncadenada.IteratorLista;
import model.data_structures.Stack.IteratorStack;

public class Queue<T> implements IQueue<T>{

	private ListaEncadenada<T> lista;

	public Queue() {
		lista = new ListaEncadenada<T>();
	}

	public ListaEncadenada<T> lista() {
		return lista;
	}


	@Override
	public boolean isEmpty() {

		return lista.getSize()==0;
	}

	@Override
	public int size() {

		return lista.getSize();
	}

	@Override
	public void enqueue(T t) {

		lista.addAtEnd(t);
	}

	@Override
	public T dequeue() {

		T eliminado = lista.darPrimero().darDato();
		lista.removeFirst();
		return eliminado;
	}

	@Override
	public IteratorQueue<T> iterator() {

		return new IteratorQueue<T>(lista.darPrimero());
	}
	public class IteratorQueue<T> implements Iterator<T>{


		private Nodo<T> proximo;


		public IteratorQueue(Nodo<T> primero) {

			proximo = primero;
		}	
		@Override
		public boolean hasNext() {
			return proximo != null;
		}

		@Override
		public T next() {
			if(proximo == null)
				throw new NoSuchElementException("No hay proximo");

			T elemento = proximo.darDato();
			proximo = proximo.darSiguiente();
			return elemento;
		}
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
}
