package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListaEncadenada<T> implements IListaEncadenada<T> {

	private ListaEncadenada<T> listaEncadenada;

	private  Nodo<T> primero;

	private Nodo<T> ultimo;

	private int size;


	public ListaEncadenada() {
		primero = null;
		ultimo = null;
		size = 0;
	}

	public Nodo<T> darPrimero(){
		return primero;
	}
	public Nodo<T> darUltimo(){
		return ultimo;
	}
	public IteratorLista<T> iterator() {

		return new IteratorLista<T>(primero);
	}
	public class IteratorLista<T> implements Iterator<T>{


		private Nodo<T> proximo;


		public IteratorLista(Nodo<T> primero) {

			proximo = primero;
		}	
		@Override
		public boolean hasNext() {
			return proximo != null;
		}

		@Override
		public T next() {
			if(proximo == null)
				throw new NoSuchElementException("No hay proximo");

			T elemento = proximo.darDato();
			proximo = proximo.darSiguiente();
			return elemento;
		}
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public Integer getSize() {

		return size;
	}


	@Override
	public boolean add(T dato) {

		boolean variable = false;
		if(dato == null)
			variable = false;
		else {
			Nodo<T> agregar = new Nodo<T>(dato);
			if(primero == null ) {
				primero = agregar;
				ultimo = primero;
				variable = true;
			}
			else{
				agregar.cambiarSiguiente(primero);
				primero = agregar;
				variable = true;
			}
			size++;
		}
		return variable;
	}
	@Override
	public boolean addAtEnd(T dato) {
		boolean variable = false;
		if(dato == null)
			variable = false;
		else {
			Nodo<T> agregar = new Nodo<T>(dato);
			if(primero == null) {
				primero = agregar;
				ultimo = primero;
				variable = true;
			}
			else {
				ultimo.cambiarSiguiente(agregar);
				ultimo = agregar;	
				variable = true;
			}
			size++;
		}
		return variable;
	}

	@Override
	public T getElement(int i) {
		IteratorLista<T> iterador = listaEncadenada.iterator();
		int pos = 0;
		if(iterador != null) {
			while(iterador.hasNext()) {
				T resp = iterador.next();
				if(pos == i){
					return resp;
				}
				pos++;
			}
		}
		return null;
	}



	@Override
	public void removeFirst(){

		if(primero == null)
			throw new IndexOutOfBoundsException("La lista está vacia");
		else {
			primero = primero.darSiguiente();
			size--;
		}


	}

	@Override
	public void removeAtEnd() {
		if(primero == null)
			throw new IndexOutOfBoundsException("La lista esta vacia");
		else if(primero.darSiguiente() == null) {
			primero = null;
			ultimo = null;
			size--;
		}
		else {
			Nodo<T> antUlt = new Nodo<T>(getElement(size-2));
			antUlt.cambiarSiguiente(null);
			ultimo = antUlt;
			size--;
		}

	}

}
