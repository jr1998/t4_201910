package model.util;

import java.util.Random;

public class Sort {
	private static Comparable [] aux;

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo ShellSort
		int length= datos.length;
		int h=1;
		while(h < length/3)
		{
			h=3*h+1;
		}
		while(h>=1)
		{
			for (int i=h;i<length; i++)
			{
				for (int j=i; j>=h && less(datos[j],datos[j-h])==true;j-=h)
				{
					exchange(datos,j,j-h);
				}
				h=h/3;
			}
		}
	}

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo MergeSort
		Comparable [] aux= new Comparable [datos.length];
		MergeSort(datos,0,datos.length-1);

	}
	private static void MergeSort(Comparable [] datos, int bajo, int alto)
	{
		if(alto<=bajo) return;
		int mitad=bajo+(alto - bajo)/2;
		MergeSort(datos, bajo, mitad);
		MergeSort(datos, mitad+1, alto);
		merge(datos,bajo,mitad,alto);
	}
	public static void merge(Comparable [] a, int lo, int mid, int hi)
	{
		if(aux!=null)
		{
			int i=lo;
			int j=mid+1;
			for (int k=lo;k <=hi;k++)
			{

				aux[k]=a[k];
			}
			for (int k=lo;k<=hi;k++)
			{
				if(i>mid)
				{
					a[k]=aux[j++];
				}
				else if(j>hi)
				{
					a[k]=aux[i++];
				}
				else if(less(aux[j],aux[i]))
				{
					a[k]=aux[j++];
				}
				else
				{
					a[k]=aux[i++];
				}
			}
		}
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos ) {

		RandomizeArray(datos);
		QuickSort(datos,0,datos.length-1);
	}
	private static void QuickSort(Comparable [] datos, int bajo, int alto)
	{
		if(alto<=bajo) return;
		int j=partition(datos, bajo, alto);
		QuickSort(datos, bajo,j-1);
		QuickSort(datos,j+1,alto);

	}
	private static int partition(Comparable [] datos, int bajo, int alto)
	{
		int i=bajo;
		int j=alto+1;
		Comparable aux1=datos[bajo];
		while(true)
		{
			while (less(datos[++i],aux1))
			{
				if(i==alto)break;
			}
			while (less(aux1,datos[--j]))
			{
				if(j==bajo)break;
			}
			if(i>=j)break;
			exchange(datos,i,j);
		}
		exchange(datos,bajo,j)	;
		return j;
	}

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	public static boolean less(Comparable v, Comparable w)
	{

		return v.compareTo(w)<0;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		Comparable t= datos[i];
		datos[i]=datos[j];
		datos[j]=t;

	}
	public static Comparable [] RandomizeArray(Comparable [] array){
		Random rgen = new Random();  // Random number generator			

		for (int i=0; i<array.length; i++) {
			int randomPosition = rgen.nextInt(array.length);
			Comparable  temp = array[i];
			array[i] = array[randomPosition];
			array[randomPosition] = temp;
		}

		return array;
	}

}
