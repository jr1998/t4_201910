package model.vo;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	// TODO Definir los atributos de una infraccion
	private int objectID;

	private String row;

	private String location;

	private int addressId;

	private int streetSegid;

	private double xCoord;

	private double yCoord;

	private String ticketType;

	private int fineAMT;

	private double totalPaid;

	private int penalty1;

	private int penalty2;

	private String accidentIndicator;

	private String ticketIssueDate;

	private String violationCode;

	private String violationDesc;

	private String rowID;

	/**
	 * Metodo constructor
	 */
	public VOMovingViolation(  )
	{
		// TODO Implementar
	}	

	public int objectId() {		
		return objectID;
	}	
	public String getRow() {
		return row;
	}
	public String getLocation() {
		return location;
	}
	public int getAddressID() {
		return addressId;
	}
	public int getStreetSegid() {
		return streetSegid;
	}
	public double getXCoord() {
		return xCoord;
	}
	public double getYCoord() {
		return yCoord;
	}
	public String getTicketType() {
		return ticketType;
	}
	public int getFineAMT() {
		return fineAMT;
	}
	public int getPenalty1() {
		return penalty1;
	}
	public int getPenalty2() {
		return penalty2;
	}
	public String getViolationCode() {
		return violationCode;
	}
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}
	public double getTotalPaid() {
		return totalPaid;
	}
	public String  getAccidentIndicator() {
		return accidentIndicator;
	}
	public String  getViolationDescription() {
		return violationDesc;
	}

	public String getRowId() {
		return rowID;
	}

	public void setObjectID(int pObjectID) {
		objectID = pObjectID;
	}
	public void setRow(String pRow) {
		row = pRow;
	}
	public void setLocation(String pLocation) {
		location = pLocation;
	}

	public void setAddressID(int pAddressID) {
		addressId = pAddressID;
	}
	public void setStreetSegid(int pStreetSegid) {
		streetSegid = pStreetSegid;
	}
	public void setXCoord(double pXCoord) {
		xCoord = pXCoord;
	}
	public void setYCoord(double pYCoord) {
		yCoord = pYCoord;
	}
	public void setTicketType(String pTicketType ) {
		ticketType = pTicketType;
	}
	public void setFineAMT(int pFineAMT) {
		fineAMT = pFineAMT;
	}
	public void setPenalty1(int pPenalty1) {
		penalty1 = pPenalty1;
	}
	public void setPenalty2(int pPenalty2) {
		penalty2 = pPenalty2;
	}
	public void setViolationCode(String pViolationCode) {
		violationCode = pViolationCode;
	}
	public void setTicketIssueDate(String pTicketIssueDate) {
		ticketIssueDate = pTicketIssueDate;
	}
	public void setTotalPaid(double d) {
		totalPaid = d;
	}
	public void setAccidentIndicator(String pAccidentIndicator) {
		accidentIndicator = pAccidentIndicator;
	}
	public void setViolationDescription(String pViolationDescription) {
		violationDesc = pViolationDescription;
	}
	public void setRowId(String pRowID) {
		rowID = pRowID;
	}


	@Override
	public int compareTo(VOMovingViolation o) {
		// TODO implementar la comparacion "natural" de la clase

		int comparacion=ticketIssueDate.compareTo(o.getTicketIssueDate());
		if (comparacion==0)
		{
			if(objectID>o.objectId())
			{
				comparacion=1;
			}
			else if(objectID<o.objectId())
			{
				comparacion=-1;
			}
			else
			{
				comparacion=0;
			}
		}
		return comparacion;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "Identificación de violación "+objectID+"y fecha en la que fue cometida "+ticketIssueDate;
	}
}
