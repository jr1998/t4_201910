package model.util;

import static org.junit.Assert.*;

import java.security.SecureRandom;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import model.util.Sort;

public class SortTest {

	// Muestra de datos a ordenar
	private Comparable[] datos;

	@Before
	public void setUp() throws Exception{
		System.out.println("Codigo de configuracion de muestra de datos a probar");
	}

	@Test
	public void testNúmeros() {
		Integer [] números = {20,3,5,8,90,23,65,89,34,6,1,2,3,6,9,32,67,5,11,25,37,49,56,65,78,82,93,103,0,2,3,4 };
		datos=números;
		int size=datos.length;

		// Prueba que los datos estan ordenados ascendentemente con el método ShellSort
		Sort.ordenarShellSort(datos);

		int TSS= datos[size/2].compareTo(datos[size-1]);
		int FSS= datos[0].compareTo(datos[size/2]);
		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",false,TSS<0);
		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",false,FSS>0);

		// Prueba que los datos estan ordenados ascendentemente con el método MergeSort
		Sort.RandomizeArray(datos);
		Sort.ordenarMergeSort(datos);
		int TMS= datos[size/2].compareTo(datos[size-1]);
		int FMS= datos[0].compareTo(datos[size/2]);

		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",true,TMS<0);
		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",false,FMS>0);
		// Prueba que los datos estan ordenados ascendentemente con el método QuickSort
		Sort.RandomizeArray(datos);
		Sort.ordenarQuickSort(datos);

		int TQS= datos[size/2].compareTo(datos[size-1]);
		int FQS= datos[0].compareTo(datos[size/2]);

		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",true,TQS<0);
		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",false,FQS>0);
	}
	public void testStrings() {
		String [] abecedario = {"W", "T", "H", "D", "E", "V", "G", "H", "Y", "N", 
				"M", "P", "B","I","O","A","Q","X","O","T","U","V","A", "E","I","Z" };
		datos=abecedario;
		int size=datos.length;

		// Prueba que los datos estan ordenados ascendentemente con el método ShellSort
		Sort.ordenarShellSort(datos);

		int TSS= datos[size/2].compareTo(datos[size-1]);
		int FSS= datos[0].compareTo(datos[size/2]);
		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",true,TSS<0);
		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",false,FSS>0);

		// Prueba que los datos estan ordenados ascendentemente con el método MergeSort
		Sort.RandomizeArray(datos);
		Sort.ordenarMergeSort(datos);
		int TMS= datos[size/2].compareTo(datos[size-1]);
		int FMS= datos[0].compareTo(datos[size/2]);

		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",true,TMS<0);
		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",false,FMS>0);
		// Prueba que los datos estan ordenados ascendentemente con el método QuickSort
		Sort.RandomizeArray(datos);
		Sort.ordenarQuickSort(datos);

		int TQS= datos[size/2].compareTo(datos[size-1]);
		int FQS= datos[0].compareTo(datos[size/2]);

		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",true,TQS<0);
		assertEquals("El algoritmo de ordenamiento no esta funcionando correctamente",false,FQS>0);
	}

}
